# SLUG

## (Simple, Laid-back Universal Game)

Copyright 1992 & 1993 By Steffan O'Sullivan  
Edited by Ann Dupuis  
This page last updated June 12, 1998

### Introduction

***SLUG*** is a free role-playing game - feel free to make copies and
give them to your friends. It was years in the making, and contains both
American and Metric units for complete user-friendliness. If you typeset
it right, you should be able to fit it on one side of a sheet of paper -
we do\!

### Chapter 1: Character Creation

Each character is described any way the player wants. Use words,
numbers, pictures, symbols - whatever. However, the Game Master should
be able to understand it, so be ready to translate obscure character
concepts before play begins. The GM can modify outrageous proposals,
suggest appropriate changes, or ban the character from a particular
adventure or campaign.

#### Examples:

"**Grok** is strong, very strong. He's never met anyone stronger. He's
not real bright, but good with his fists and clubs and throwing stones.
He doesn't speak much."

"**Andarra** is a wizard who knows how to create an illusion of almost
anything. She can also teleport and sometimes read thoughts, but has
trouble with the latter. On a 3-18 scale, her stats are Intelligence 17,
Dexterity 10, Strength 9, Constitution 11, Perception 13, Will 15, Charm
12, Appearance 14."

"**François** is dashing, handsome, athletic, and highly skilled with
his sword. He is suave and polished in his manner, and is skilled at
witty repartee. He is very weak in the sciences, though. He is proud,
quick-tempered, and extremely loyal to the king. He is fond of brawling
in bars, carousing with friends, and wooing lovely ladies (not
necessarily in that order)."

"**Juanita Martinez** is a detective who investigates the occult. She
has an unreliable ability (40% chance) to detect psionic activity up to
300 meters (330 yards). She is also capable of some slight telekinesis:
she can move objects of up to 1/2 pound (1/4 kg) very slowly - 6 inches
(15 cm) per second. She is highly skilled (i.e., better than 70% chance)
at Research, Pistol Use, Driving and Bamboozling her way into places she
has no business. She is moderately skilled at Swimming, Moving Quietly,
Lockpicking, Mexican Cooking, and Photography. She is knowledgeable
about New York, Occult Matters, Computer Use, Obscure Religions and
Cults, and Standard Police Procedures. Juanita is attractive, can be
jealous, and is more curious than is safe at times. She has a few
friends in high places."

"**Captain Horatio Quest** is a war hero, decorated many times in the
war against the Shleeshlokk Empire. His piloting skills are legendary.
He's a crack shot with a blast rifle, and has extensive experience with
personal combat in zero-G. His intellectual abilities run more toward
tactics and strategy than formal learning. Although in his mid-fifties,
Quest is still handsome (due to the heroic efforts of reconstructive
surgeons) and physically fit."

### Chapter 2: Action Resolution

When the GM asks a player to roll some dice, the player should roll some
dice. The GM can specify the number, size, and shape of the dice, or
just let the player choose. The GM will assess the result based on how
far above or below the average the result is - the higher, the better
(unless the GM says otherwise). The GM rolls in secret on occasion,
mostly for information rolls.

#### Examples:

**Player:** "Andarra creates an illusion of a large, angry dragon."

**GM:** "Roll three six-sided dice."

(Andarra's player rolls a 4 and two 5s.)

**GM** (comparing the roll to an average total of 10-11): "The bandits
run in terror of the dragon that appears beside Andarra\!"

-----

**Player:** "Grok tries to put the pieces of the radio back together."

**GM:** "Okay, roll some dice." (Grok's player rolls a 1 on a d4.) "The
scattered pieces confuse Grok so much, he gets a headache."

-----

**Player:** "Does François recognize the lady's accent?"

**GM:** (secretly rolling two 10-sided dice, getting a 3 and a 5, and
reading them as percentiles for a result of 35%): "You can't quite place
it - definitely not Italian, though, and probably not Spanish."

#### Modifiers:

The GM may assign a modifier to the Action Resolution Roll. A modifier
may be of any appropriate amount, up or down, depending on the
circumstances (and on the number and type of dice rolled). For example,
a magic sword may give a bonus in combat; scaling a cliff is easier with
proper climbing gear; and a character described as a master of a skill
gets a bonus when using that skill. Conversely, a wound might mean a
penalty to most actions; and fighting in poor conditions will probably
subtract something from a combat roll.

### Chapter 3: Character Development

As the game progresses, a character will encounter situations that will
test things that weren't written down at first. The player should
explain to the GM how good the character is in such a situation.

#### Examples:

> "*Oh, yes, Grok's climbed lots of cliffs before,*"

or

> "*François might recognize a Spanish or English accent with luck. He
> would recognize an Italian accent easily, though - there are Italians
> in the Musketeers.*"

The GM will probably accept an explanation if it seems in keeping with
the original character concept. However, if it's not written down, the
GM doesn't have to accept an explanation, and can decide how good the
character **really** is.

With the GM's permission, such traits can be added, or existing traits
or skills improved, **after** the gaming session. For example, François
might spend a week "negotiating" a non-existent deal with an English
merchant just to have a better chance of recognizing an English accent
in the future. It is doubtful, however, that Grok will ever get the
radio to work again.

**The End.**
