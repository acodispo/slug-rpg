# README

This repository contains a copy in Markdown format of Steffan O'Sullivan's RPG SLUG.

The original is here: <http://www.panix.com/~sos/rpg/slug.html>

The first sentence of SLUG is: 

> "SLUG is a free role-playing game - feel free to make copies and give them to your friends."

I am distributing it here for that purpose.
